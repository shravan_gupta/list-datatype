/*
main.c

Author: Shravan Gupta
Date: 21-5-20
*/

#include "list.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

// Macro for custom testing; does exit(1) on failure.
#define CHECK(condition) do{ \
    if (!(condition)) { \
        printf("ERROR: %s (@%d): failed condition \"%s\"\n", __func__, __LINE__, #condition); \
        exit(1);\
    }\
} while(0)

// For checking the "free" function called
static int complexTestFreeCounter = 0;
static void complexTestFreeFn(void* pItem) 
{
    CHECK(pItem != NULL);
    complexTestFreeCounter++;
}

// For searching
static bool itemEquals(void* pItem, void* pArg) 
{
    return (pItem == pArg);
}

// Support:  Verify List
static bool listEquals(List* pList, void*expectedItemsNullTerm[])
{
    // Ensure each exected item is there
    List_first(pList);
    for (int i = 0; expectedItemsNullTerm[i] != NULL; i++) {
        if (List_curr(pList) != expectedItemsNullTerm[i]) {
            return false;
        }
        List_next(pList);
    }

    // Ensure no extra items there
    if (List_curr(pList) != NULL) {
        return false;
    }

    // Made it!
    return true;
}

// SUPPORT: Functions to pass to module
static int freeCallbackCount = 0;
static void freeCallback(void* pNode) 
{
    freeCallbackCount++;
}

static int consumeAllFreeLists()
{
    // Allocate as many nodes as we can!
    int numFreeLists = 0;
    for (int i = 0; i < LIST_MAX_NUM_HEADS + 1; i++) {
        List* pLists = List_create();
        if (pLists == NULL) {
            numFreeLists = i;
            break;
        }
    }

    CHECK(numFreeLists <= LIST_MAX_NUM_NODES);
    // Intentionally leak the resources
    return numFreeLists;
}

static int consumeAllFreeNodes(List* pList)
{
    int someGarbageItemInStack = 42;
    // Allocate as many as we can!
    // Do one more than allowed just to check!
    int numFreeNodes = 0;
    for (int i = 0; i < LIST_MAX_NUM_NODES + 1; i++) {
        int status = List_add(pList, &someGarbageItemInStack);
        if (status == -1) {
            numFreeNodes = i;
            break;
        }
    }

    CHECK(numFreeNodes <= LIST_MAX_NUM_NODES);
    // Intentionally leak the resources
    return numFreeNodes;
}

static void testComplex()
{
    // Create an empty list
    List* pList = List_create();
    CHECK(pList != NULL);
    CHECK(List_count(pList) == 0);

    List* pList1 = List_create();
    CHECK(pList != NULL);
    CHECK(List_count(pList1) == 0);

    //Checking functionality on an empty list
    CHECK(List_first(pList) == NULL);
    CHECK(List_last(pList) == NULL);
	CHECK(List_next(pList) == NULL);
	CHECK(List_prev(pList) == NULL);
    
    // Remove functionality on an empty list
    CHECK(List_first(pList) == NULL);
    CHECK(List_remove(pList) == NULL);
	CHECK(List_curr(pList) == NULL);
    CHECK(List_count(pList) == 0);

    // Trim functionality on an empty list
    CHECK(List_trim(pList) == NULL);
    CHECK(List_curr(pList) == NULL);
    CHECK(List_count(pList) == 0);

    // Free functionality on an empty list
    complexTestFreeCounter = 0;
    List_free(pList, complexTestFreeFn);
    CHECK(complexTestFreeCounter == 0);
    CHECK(List_count(pList) == 0);
    CHECK(List_curr(pList) == NULL);

    complexTestFreeCounter = 0;
    List_free(pList1, complexTestFreeFn);
    CHECK(complexTestFreeCounter == 0);
    CHECK(List_count(pList1) == 0);
    CHECK(List_curr(pList1) == NULL);


	//instructor checkkkkkkkkkkkkkkkkkkkkkkkk
	// List* pList0 = List_create();
    // int junkData = 42;
    // for (int i = 0; i < LIST_MAX_NUM_NODES; i++) {
        // CHECK(List_insert(pList0, &junkData) == 0);
    // }
    // CHECK(List_count(pList0) == LIST_MAX_NUM_NODES);

    // // Free all
    // for (int i = 0; i < LIST_MAX_NUM_NODES; i++) {
        // CHECK(List_trim(pList0) == &junkData);
    // }
    // CHECK(List_count(pList0) == 0);

    // // Reuse all
    // for (int i = 0; i < LIST_MAX_NUM_NODES; i++) {
        // CHECK(List_insert(pList0, &junkData) == 0);
    // }
    // CHECK(List_count(pList0) == LIST_MAX_NUM_NODES);
    // // .. should be no more!
    // CHECK(List_append(pList0, &junkData) == -1);

	
    // List* pLists[LIST_MAX_NUM_HEADS];
    // for (int i = 0; i < LIST_MAX_NUM_HEADS; i++) {
        // pLists[i] = List_create();
        // CHECK(pLists[i] != NULL);
    // }
    // CHECK(List_create() == NULL);

    // // Free first
    // List_free(pLists[0], freeCallback);

    // // Reuse
    // CHECK(List_create() != NULL);
    // CHECK(List_create() == NULL);
	
    // List* pLists[LIST_MAX_NUM_HEADS];
    // for (int i = 0; i < LIST_MAX_NUM_HEADS; i++) {
        // pLists[i] = List_create();
        // CHECK(pLists[i] != NULL);
    // }
    // CHECK(List_create() == NULL);

    // // Free some
    // int countFreed = 0;
    // for (int i = 0; i < LIST_MAX_NUM_HEADS; i+=3) {
        // List_free(pLists[i], freeCallback);
        // countFreed++;
    // }

    // // Reuse
    // for (int i = 0; i < countFreed; i++) {
        // CHECK(List_create() != NULL);
    // }
    // CHECK(List_create() == NULL);
	
    // List* pList0 = List_create();
    // consumeAllFreeLists();
    // List_free(pList0, freeCallback);
    // CHECK(List_create() != NULL);
	
    // List* pList0 = List_create();
    // consumeAllFreeNodes(pList0);
    // CHECK(List_add(pList0, NULL) == -1);

    //-----------------------Add-------------------------------

    // Add items to the list
    int added = 1;
    CHECK(List_add(pList, &added) == 0);
    CHECK(List_count(pList) == 1);
    CHECK(List_curr(pList) == &added);
    int added1 = 2;
    CHECK(List_add(pList, &added1) == 0);
    CHECK(List_count(pList) == 2);
    CHECK(List_curr(pList) == &added1);
    int added2 = 3;
    CHECK(List_add(pList, &added2) == 0);
    CHECK(List_count(pList) == 3);
    CHECK(List_curr(pList) == &added2);
	
	//instructor checkkkkkkkkkkkkkkkkkkkkkkkk
    // double newValue = 25;
    // List_first(pList);
    // CHECK(List_next(pList) == &added1);
    // CHECK(List_insert(pList, &newValue) == 0);
    // CHECK(listEquals(pList, (void*[]){&added, &newValue, &added1, &added2, NULL}));

    // // Next through it all (from before list)
    // CHECK(List_first(pList) == &added);
    // CHECK(List_prev(pList) == NULL);
    // CHECK(List_next(pList) == &added);
    // CHECK(List_next(pList) == &newValue);
    // CHECK(List_next(pList) == &added1);
    // CHECK(List_next(pList) == &added2);
    // CHECK(List_next(pList) == NULL);

    // Prev through it all starting from past end
    CHECK(List_last(pList) == &added2);
    CHECK(List_next(pList) == NULL);
    CHECK(List_prev(pList) == &added2);
    CHECK(List_prev(pList) == &added1);
    CHECK(List_prev(pList) == &added);
    CHECK(List_prev(pList) == NULL);
    CHECK(List_prev(pList) == NULL);

    //Check with another list
    CHECK(List_add(pList1, &added) == 0);
    CHECK(List_count(pList1) == 1);
    CHECK(List_curr(pList1) == &added);
    CHECK(List_add(pList1, &added1) == 0);
    CHECK(List_count(pList1) == 2);
    CHECK(List_curr(pList1) == &added1);

    // Next through it all (from before list)
    CHECK(List_first(pList1) == &added);
    CHECK(List_prev(pList1) == NULL);
    CHECK(List_next(pList1) == &added);
    CHECK(List_next(pList1) == &added1);
    CHECK(List_next(pList1) == NULL);
    CHECK(List_next(pList1) == NULL);

    // Prev through it all starting from past end
    CHECK(List_last(pList1) == &added1);
    CHECK(List_next(pList1) == NULL);
    CHECK(List_prev(pList1) == &added1);
    CHECK(List_prev(pList1) == &added);
    CHECK(List_prev(pList1) == NULL);
    CHECK(List_prev(pList1) == NULL);

    // Free elements of list
    complexTestFreeCounter = 0;
    List_free(pList, complexTestFreeFn);
    CHECK(complexTestFreeCounter == 3); 
    CHECK(pList->active == false);
    CHECK(List_count(pList) == 0);
    CHECK(List_curr(pList) == NULL);

    complexTestFreeCounter = 0;
    List_free(pList1, complexTestFreeFn);
    CHECK(complexTestFreeCounter == 2); 
    CHECK(pList1->active == false);
    CHECK(List_count(pList1) == 0);
    CHECK(List_curr(pList1) == NULL);

    //-------------------------Insert-----------------------------

    pList = List_create();
    CHECK(pList!= NULL);
    CHECK(List_count(pList) == 0);
    pList1 = List_create();
    CHECK(pList1 != NULL);
    CHECK(List_count(pList1) == 0);

    // Insert items to the list
    int inserted = 1;
    CHECK(List_insert(pList1, &inserted) == 0);
    CHECK(List_count(pList1) == 1);
    CHECK(List_curr(pList1) == &inserted);
    int inserted1 = 2;
    CHECK(List_insert(pList1, &inserted1) == 0);
    CHECK(List_count(pList1) == 2);
    CHECK(List_curr(pList1) == &inserted1);
    int inserted2 = 3;
    CHECK(List_insert(pList1, &inserted2) == 0);
    CHECK(List_count(pList1) == 3);
    CHECK(List_curr(pList1) == &inserted2);
	
	//instructor checkkkkkkkkkkkkkkkkkkkkkkkk
    // double newValue = 25;
    // List_first(pList1);
    // CHECK(List_next(pList1) == &inserted1);
    // CHECK(List_insert(pList1, &newValue) == 0);
    // CHECK(listEquals(pList1, (void*[]){&inserted2, &newValue, &inserted1, &inserted, NULL}));	

    // Next through it all (from before list)
    CHECK(List_first(pList1) == &inserted2);
    CHECK(List_prev(pList1) == NULL);
    CHECK(List_next(pList1) == &inserted2);
    CHECK(List_next(pList1) == &inserted1);
    CHECK(List_next(pList1) == &inserted);
    CHECK(List_next(pList1) == NULL);
    CHECK(List_next(pList1) == NULL);

    // Prev through it all starting from past end
    CHECK(List_last(pList1) == &inserted);
    CHECK(List_next(pList1) == NULL);
    CHECK(List_prev(pList1) == &inserted);
    CHECK(List_prev(pList1) == &inserted1);
    CHECK(List_prev(pList1) == &inserted2);
    CHECK(List_prev(pList1) == NULL);
    CHECK(List_prev(pList1) == NULL);

    //Check with another list
    CHECK(List_insert(pList, &inserted) == 0);
    CHECK(List_count(pList) == 1);
    CHECK(List_curr(pList) == &inserted);
    CHECK(List_insert(pList, &inserted1) == 0);
    CHECK(List_count(pList) == 2);
    CHECK(List_curr(pList) == &inserted1);

    // Next through it all (from before list)
    CHECK(List_first(pList) == &inserted1);
    CHECK(List_prev(pList) == NULL);
    CHECK(List_next(pList) == &inserted1);
    CHECK(List_next(pList) == &inserted);
    CHECK(List_next(pList) == NULL);
    CHECK(List_next(pList) == NULL);

    // Prev through it all starting from past end
    CHECK(List_last(pList) == &inserted);
    CHECK(List_next(pList) == NULL);
    CHECK(List_prev(pList) == &inserted);
    CHECK(List_prev(pList) == &inserted1);
    CHECK(List_prev(pList) == NULL);
    CHECK(List_prev(pList) == NULL);

    // Free elements of list
    complexTestFreeCounter = 0;
    List_free(pList1, complexTestFreeFn);
    CHECK(complexTestFreeCounter == 3); 
    CHECK(pList1->active == false);
    CHECK(List_count(pList1) == 0);
    CHECK(List_curr(pList1) == NULL);

    complexTestFreeCounter = 0;
    List_free(pList, complexTestFreeFn);
    CHECK(complexTestFreeCounter == 2); 
    CHECK(pList->active == false);
    CHECK(List_count(pList) == 0);
    CHECK(List_curr(pList) == NULL);

    //-------------------------Append--------------------------  

    pList = List_create();
    CHECK(pList != NULL);
    CHECK(List_count(pList) == 0);
    pList1 = List_create();
    CHECK(pList1 != NULL);
    CHECK(List_count(pList1) == 0);

    // Append items to the list
    int appended = 1;
    CHECK(List_append(pList1, &appended) == 0);
    CHECK(List_count(pList1) == 1);
    CHECK(List_curr(pList1) == &appended);
    int appended1 = 2;
    CHECK(List_append(pList1, &appended1) == 0);
    CHECK(List_count(pList1) == 2);
    CHECK(List_curr(pList1) == &appended1);
    int appended2 = 3;
    CHECK(List_append(pList1, &appended2) == 0);
    CHECK(List_count(pList1) == 3);
    CHECK(List_curr(pList1) == &appended2);

    // Next through it all (from before list)
    CHECK(List_first(pList1) == &appended);
    CHECK(List_prev(pList1) == NULL);
    CHECK(List_next(pList1) == &appended);
    CHECK(List_next(pList1) == &appended1);
    CHECK(List_next(pList1) == &appended2);
    CHECK(List_next(pList1) == NULL);
    CHECK(List_next(pList1) == NULL);

    // Prev through it all starting from past end
    CHECK(List_last(pList1) == &appended2);
    CHECK(List_next(pList1) == NULL);
    CHECK(List_prev(pList1) == &appended2);
    CHECK(List_prev(pList1) == &appended1);
    CHECK(List_prev(pList1) == &appended);
    CHECK(List_prev(pList1) == NULL);
    CHECK(List_prev(pList1) == NULL);

    //Check with another list
    CHECK(List_append(pList, &appended) == 0);
    CHECK(List_count(pList) == 1);
    CHECK(List_curr(pList) == &appended);
    CHECK(List_append(pList, &appended1) == 0);
    CHECK(List_count(pList) == 2);
    CHECK(List_curr(pList) == &appended1);

    // Next through it all (from before list)
    CHECK(List_first(pList) == &appended);
    CHECK(List_prev(pList) == NULL);
    CHECK(List_next(pList) == &appended);
    CHECK(List_next(pList) == &appended1);
    CHECK(List_next(pList) == NULL);
    CHECK(List_next(pList) == NULL);

    // Prev through it all starting from past end
    CHECK(List_last(pList) == &appended1);
    CHECK(List_next(pList) == NULL);
    CHECK(List_prev(pList) == &appended1);
    CHECK(List_prev(pList) == &appended);
    CHECK(List_prev(pList) == NULL);
    CHECK(List_prev(pList) == NULL);

    // Free elements of list
    complexTestFreeCounter = 0;
    List_free(pList1, complexTestFreeFn);
    CHECK(complexTestFreeCounter == 3); 
    CHECK(pList1->active == false);
    CHECK(List_count(pList1) == 0);
    CHECK(List_curr(pList1) == NULL);

    complexTestFreeCounter = 0;
    List_free(pList, complexTestFreeFn);
    CHECK(complexTestFreeCounter == 2); 
    CHECK(pList->active == false);
    CHECK(List_count(pList) == 0);
    CHECK(List_curr(pList) == NULL);

    //-------------------------Prepend--------------------------
    
    pList = List_create();
    CHECK(pList != NULL);
    CHECK(List_count(pList) == 0);
    pList1 = List_create();
    CHECK(pList1!= NULL);
    CHECK(List_count(pList1) == 0);

    // Append items to the list
    int prepended = 1;
    CHECK(List_prepend(pList1, &prepended) == 0);
    CHECK(List_count(pList1) == 1);
    CHECK(List_curr(pList1) == &prepended);
    int prepended1 = 2;
    CHECK(List_prepend(pList1, &prepended1) == 0);
    CHECK(List_count(pList1) == 2);
    CHECK(List_curr(pList1) == &prepended1);
    int prepended2 = 3;
    CHECK(List_prepend(pList1, &prepended2) == 0);
    CHECK(List_count(pList1) == 3);
    CHECK(List_curr(pList1) == &prepended2);

    // Next through it all (from before list)
    CHECK(List_first(pList1) == &prepended2);
    CHECK(List_prev(pList1) == NULL);
    CHECK(List_next(pList1) == &prepended2);
    CHECK(List_next(pList1) == &prepended1);
    CHECK(List_next(pList1) == &prepended);
    CHECK(List_next(pList1) == NULL);
    CHECK(List_next(pList1) == NULL);

    // Prev through it all starting from past end
    CHECK(List_last(pList1) == &prepended);
    CHECK(List_next(pList1) == NULL);
    CHECK(List_prev(pList1) == &prepended);
    CHECK(List_prev(pList1) == &prepended1);
    CHECK(List_prev(pList1) == &prepended2);
    CHECK(List_prev(pList1) == NULL);
    CHECK(List_prev(pList1) == NULL);

    //Check with another list
    CHECK(List_prepend(pList, &prepended) == 0);
    CHECK(List_count(pList) == 1);
    CHECK(List_curr(pList) == &prepended);
    CHECK(List_prepend(pList, &prepended1) == 0);
    CHECK(List_count(pList) == 2);
    CHECK(List_curr(pList) == &prepended1);

    // Next through it all (from before list)
    CHECK(List_first(pList) == &prepended1);
    CHECK(List_prev(pList) == NULL);
    CHECK(List_next(pList) == &prepended1);
    CHECK(List_next(pList) == &prepended);
    CHECK(List_next(pList) == NULL);
    CHECK(List_next(pList) == NULL);

    // Prev through it all starting from past end
    CHECK(List_last(pList) == &prepended);
    CHECK(List_next(pList) == NULL);
    CHECK(List_prev(pList) == &prepended);
    CHECK(List_prev(pList) == &prepended1);
    CHECK(List_prev(pList) == NULL);
    CHECK(List_prev(pList) == NULL);

    // Free elements of list
    complexTestFreeCounter = 0;
    List_free(pList1, complexTestFreeFn);
    CHECK(complexTestFreeCounter == 3); 
    CHECK(pList1->active == false);
    CHECK(List_count(pList1) == 0);
    CHECK(List_curr(pList1) == NULL);

    complexTestFreeCounter = 0;
    List_free(pList, complexTestFreeFn);
    CHECK(complexTestFreeCounter == 2); 
    CHECK(pList->active == false);
    CHECK(List_count(pList) == 0);
    CHECK(List_curr(pList) == NULL);

    //-------------------------Remove/Trim/Free--------------------------

    //Case 1: Remove
    pList = List_create();
    CHECK(pList != NULL);
    CHECK(List_count(pList) == 0);
    CHECK(List_add(pList, &added) == 0);
	// CHECK(List_add(pList, &added1) == 0);
	// CHECK(List_add(pList, &added2) == 0);
    CHECK(List_insert(pList, &inserted) == 0);
    CHECK(List_prepend(pList, &prepended) == 0);
    //CHECK(List_append(pList, &appended) == 0);
    // //Remove the first
    // CHECK(List_remove(pList) == &appended);
	// CHECK(List_curr(pList) == NULL);
    // CHECK(List_count(pList) == 3);
    // //Remove the second
    // CHECK(List_first(pList) == &prepended);
    // CHECK(List_remove(pList) == &prepended);
	// CHECK(List_curr(pList) == &inserted);
    // CHECK(List_count(pList) == 2);
    // //Remove the third
    // CHECK(List_remove(pList) == &inserted);
	// CHECK(List_curr(pList) == &added);
    // CHECK(List_count(pList) == 1);
    // //Remove the last
    // CHECK(List_remove(pList) == &added);
	// CHECK(List_curr(pList) == NULL);
    // CHECK(List_count(pList) == 0);
	
	//instructor checkkkkkkkkkkkkkkkkkkkkkkkk
    List_first(pList);
    List_next(pList);
    CHECK(List_remove(pList) == &inserted);
    CHECK(List_count(pList) == 2);
    CHECK(List_remove(pList) == &added);
    CHECK(listEquals(pList, (void*[]){&prepended, NULL}));
	

    //Case 2: Trim
    pList1 = List_create();
    CHECK(pList1 != NULL);
    CHECK(pList1 != NULL);
    CHECK(List_insert(pList1, &inserted) == 0);
    CHECK(List_add(pList1, &added) == 0);
    CHECK(List_append(pList1, &appended) == 0);
    CHECK(List_prepend(pList1, &prepended) == 0);
    //Trim
    CHECK(List_trim(pList1) == &appended);
    CHECK(List_curr(pList1) == &added);
    CHECK(List_count(pList1) == 3);
    //Trim
    CHECK(List_trim(pList1) == &added);
    CHECK(List_curr(pList1) == &inserted);
    CHECK(List_count(pList1) == 2);
    //Trim
    CHECK(List_trim(pList1) == &inserted);
    CHECK(List_curr(pList1) == NULL);//check &prepended
    CHECK(List_count(pList1) == 1);
    //Trim
    CHECK(List_trim(pList1) == &prepended);
    CHECK(List_curr(pList1) == NULL);
    CHECK(List_count(pList1) == 0);

    //Case 3: Remove-Trim-Remove-Trim
    pList = List_create();
    CHECK(pList != NULL);
    CHECK(List_prepend(pList, &prepended) == 0);
    CHECK(List_prepend(pList, &prepended1) == 0);
    CHECK(List_append(pList, &appended) == 0);
    CHECK(List_append(pList, &appended1) == 0);
    //Remove the first
    CHECK(List_first(pList) == &prepended1);
    CHECK(List_remove(pList) == &prepended1);
	CHECK(List_curr(pList) == &prepended);
    CHECK(List_count(pList) == 3);
    //Trim the last
    CHECK(List_trim(pList) == &appended1);
    CHECK(List_curr(pList) == NULL); //check &appended
    CHECK(List_count(pList) == 2);
    //Remove the third
    CHECK(List_last(pList) == &appended);
    CHECK(List_remove(pList) == &appended);
	CHECK(List_curr(pList) == NULL);
    CHECK(List_count(pList) == 1);
    //Trim the second that is also the last
    CHECK(List_last(pList) == &prepended);
    CHECK(List_trim(pList) == &prepended);
    CHECK(List_curr(pList) == NULL);
    CHECK(List_count(pList) == 0);

    //Case 4: Trim-Remove-Trim-Remove
    pList1 = List_create();
    CHECK(pList1 != NULL);
    CHECK(pList1 != NULL);
    CHECK(List_count(pList1) == 0);
    CHECK(List_add(pList1, &added) == 0);
    CHECK(List_insert(pList1, &inserted) == 0);
    CHECK(List_prepend(pList1, &prepended) == 0);
    CHECK(List_append(pList1, &appended) == 0);
    //Trim the last
    CHECK(List_trim(pList1) == &appended);
    CHECK(List_curr(pList1) == &added);
    CHECK(List_count(pList1) == 3);
    //Remove the first
    CHECK(List_first(pList1) == &prepended);
    CHECK(List_remove(pList1) == &prepended);
	CHECK(List_curr(pList1) == &inserted);
    CHECK(List_count(pList1) == 2);
    //Trim 
    CHECK(List_trim(pList1) == &added);
    CHECK(List_curr(pList1) == NULL); //check &inserted
    CHECK(List_count(pList1) == 1);
    //Remove
    CHECK(List_first(pList1) == &inserted);
    CHECK(List_last(pList1) == &inserted);
    CHECK(List_remove(pList1) == &inserted);
	CHECK(List_curr(pList1) == NULL);
    CHECK(List_count(pList1) == 0);

    //Case 5: Remove-Remove-Remove-Free
    pList = List_create();
    CHECK(pList!= NULL);
    CHECK(pList != NULL);
    CHECK(List_count(pList) == 0);
    CHECK(List_add(pList, &added) == 0);
    CHECK(List_insert(pList, &inserted) == 0);
    CHECK(List_prepend(pList, &prepended) == 0);
    CHECK(List_append(pList, &appended) == 0);
    //Remove the first
    CHECK(List_first(pList) == &prepended);
    CHECK(List_remove(pList) == &prepended);
	CHECK(List_curr(pList) == &inserted);
    CHECK(List_count(pList) == 3);
    //Remove the second
    CHECK(List_first(pList) == &inserted);
    CHECK(List_remove(pList) == &inserted);
	CHECK(List_curr(pList) == &added);
    CHECK(List_count(pList) == 2);
    //Remove the third
    CHECK(List_first(pList) == &added);
    CHECK(List_remove(pList) == &added);
	CHECK(List_curr(pList) == &appended);
    CHECK(List_count(pList) == 1);
    // Free elements of list
    complexTestFreeCounter = 0;
    List_free(pList, complexTestFreeFn);
    CHECK(complexTestFreeCounter == 1); 
    CHECK(pList->active == false);
    CHECK(List_count(pList) == 0);
    CHECK(List_curr(pList) == NULL);   

    //Case 6: Trim-Trim-Trim-Free
    pList = List_create();
    CHECK(pList != NULL);
    CHECK(pList != NULL);
    CHECK(List_insert(pList, &inserted) == 0);
    CHECK(List_add(pList, &added) == 0);
    CHECK(List_append(pList, &appended) == 0);
    CHECK(List_prepend(pList, &prepended) == 0);
    //Trim
    CHECK(List_trim(pList) == &appended);
    CHECK(List_curr(pList) == &added);
    CHECK(List_count(pList) == 3);
    //Trim
    CHECK(List_trim(pList) == &added);
    CHECK(List_curr(pList) == &inserted);
    CHECK(List_count(pList) == 2);
    //Trim
    CHECK(List_trim(pList) == &inserted);
    CHECK(List_curr(pList) == NULL);//check &prepended
    CHECK(List_count(pList) == 1);
    // Free elements of list
    complexTestFreeCounter = 0;
    List_free(pList, complexTestFreeFn);
    CHECK(complexTestFreeCounter == 1); 
    CHECK(pList->active == false);
    CHECK(List_count(pList) == 0);
    CHECK(List_curr(pList) == NULL);   
    
    //Case 7: Trim-Remove-Free
    pList1 = List_create();
    CHECK(pList1 != NULL);
    CHECK(List_prepend(pList1, &prepended) == 0);
    CHECK(List_prepend(pList1, &prepended1) == 0);
    CHECK(List_append(pList1, &appended) == 0);
    CHECK(List_append(pList1, &appended1) == 0);
    //Trim the last
    CHECK(List_trim(pList1) == &appended1);
    CHECK(List_curr(pList1) == &appended);
    CHECK(List_count(pList1) == 3);
    //Remove the first
    CHECK(List_first(pList1) == &prepended1);
    CHECK(List_remove(pList1) == &prepended1);
	CHECK(List_curr(pList1) == &prepended);
    CHECK(List_count(pList1) == 2);
    // Free elements of list
    complexTestFreeCounter = 0;
    List_free(pList1, complexTestFreeFn);
    CHECK(complexTestFreeCounter == 2); 
    CHECK(pList1->active == false);
    CHECK(List_count(pList1) == 0);
    CHECK(List_curr(pList1) == NULL);     

    //Case 8: Remove-Trim-Free
    pList = List_create();
    CHECK(pList != NULL);
    CHECK(List_prepend(pList, &prepended) == 0);
    CHECK(List_prepend(pList, &prepended1) == 0);
    CHECK(List_append(pList, &appended) == 0);
    CHECK(List_append(pList, &appended1) == 0);
    //Remove the first
    CHECK(List_first(pList) == &prepended1);
    CHECK(List_remove(pList) == &prepended1);
	CHECK(List_curr(pList) == &prepended);
    CHECK(List_count(pList) == 3);
    //Trim the last
    CHECK(List_trim(pList) == &appended1);
    CHECK(List_curr(pList) == NULL); //check &appended
    CHECK(List_count(pList) == 2);
    // Free elements of list
    complexTestFreeCounter = 0;
    List_free(pList, complexTestFreeFn);
    CHECK(complexTestFreeCounter == 2); 
    CHECK(pList->active == false);
    CHECK(List_count(pList) == 0);
    CHECK(List_curr(pList) == NULL);  

    //Case 9: Free
    pList = List_create();
    CHECK(pList != NULL);
    CHECK(pList != NULL);
    CHECK(List_insert(pList, &inserted) == 0);
    CHECK(List_add(pList, &added) == 0);
    CHECK(List_append(pList, &appended) == 0);
    CHECK(List_prepend(pList, &prepended) == 0);
    //Free elements of list
    CHECK(List_first(pList) == &prepended);
    complexTestFreeCounter = 0;
    List_free(pList, complexTestFreeFn);
    CHECK(complexTestFreeCounter == 4); 
    CHECK(pList->active == false);
    CHECK(List_count(pList) == 0);
    CHECK(List_curr(pList) == NULL);  

    //If we try to remove or trim after free we should get NULL
    CHECK(List_remove(pList) == NULL);
    CHECK(List_trim(pList) == NULL);

    //-------------------------Concat--------------------------

    int one = 1;
    int two = 2;
    int three = 3;
    int four = 4;

    List* pList4 = List_create();
    CHECK(pList4 != NULL);
    CHECK(List_count(pList4) == 0);
    List* pList5 = List_create();
    CHECK(pList5 != NULL);
    CHECK(List_count(pList5) == 0); 

    // When list1 and list2 both are empty
    List_concat(pList4, pList5);
    CHECK(pList4 != NULL);
    CHECK(List_count(pList4) == 0);

    // When list1 empty and list2 non-empty
    pList4 = List_create();
    pList5 = List_create();
    CHECK(List_add(pList5, &three) == 0);
    CHECK(List_add(pList5, &four) == 0);
    CHECK(List_count(pList5) == 2);

    List_concat(pList4, pList5);
    CHECK(List_count(pList4) == 2);
    CHECK(List_first(pList4) == &three);
    CHECK(List_last(pList4) == &four);
	
	//instructor checkkkkkkkkkkkkkkkkkkkkkkkk
	// List* pList0 = List_create();
    // List* pList2 = List_create();
    // int alpha = 1;
    // int beta = 2;
    // List_add(pList2, &alpha);
    // List_add(pList2, &beta);
    // List_first(pList2);
    
    // List_concat(pList0, pList2);
    // CHECK(List_count(pList0) == 2);
    // CHECK(List_first(pList0) == &alpha);
    // CHECK(List_last(pList0) == &beta);
    // CHECK(listEquals(pList0, (void*[]){&alpha, &beta, NULL}));

    //When list1 non-empty and list2 empty
    pList4 = List_create();
    pList5 = List_create();
    CHECK(List_add(pList4, &one) == 0);
    CHECK(List_add(pList4, &two) == 0);
    CHECK(List_count(pList4) == 2);

    List_concat(pList4, pList5);
    CHECK(List_count(pList4) == 2);
    CHECK(List_first(pList4) == &one);
    CHECK(List_last(pList4) == &two);

    //When list1 and list2 both are non-empty
    pList4 = List_create();
    pList5 = List_create();
    CHECK(List_add(pList4, &one) == 0);
    CHECK(List_add(pList4, &two) == 0);
    CHECK(List_add(pList5, &three) == 0);
    CHECK(List_add(pList5, &four) == 0);
    CHECK(List_count(pList4) == 2);
    CHECK(List_count(pList5) == 2);

    List_concat(pList4, pList5);
    CHECK(List_count(pList4) == 4);
    CHECK(List_first(pList4) == &one);
    CHECK(List_last(pList4) == &four);
    CHECK(List_count(pList5) == 0);

    //-------------------------Search--------------------------

    List* pList6 = List_create();
    CHECK(pList6 != NULL);
    CHECK(List_count(pList6) == 0);

    //When list is empty
    CHECK(List_search(pList6, itemEquals, &one) == NULL);

    //When there's just one element and that is what we are searching
    CHECK(List_add(pList6, &one) == 0);
    CHECK(List_search(pList6, itemEquals, &one) == &one);
    CHECK(List_curr(pList6) == &one);   
    
    //When there's just one element and that is not what we are searching
    List_first(pList6);
    CHECK(List_search(pList6, itemEquals, &two) != &two);

    //When the list if filled and the element being searched is there
    List_first(pList4);
    CHECK(List_search(pList4, itemEquals, &two) == &two);
    CHECK(List_curr(pList4) == &two);   
    CHECK(List_search(pList4, itemEquals, &two) == &two);
    CHECK(List_curr(pList4) == &two);  
    CHECK(List_search(pList4, itemEquals, &four) == &four);
    CHECK(List_curr(pList4) == &four);

    //Curr has iterated through the array, hence we get NULL  
    CHECK(List_search(pList4, itemEquals, &one) == NULL);
    CHECK(List_search(pList4, itemEquals, &four) == NULL);
}

int main(int argCount, char *args[]) 
{
    testComplex();

    // We got here?!? PASSED!
    printf("********************************\n");
    printf("           PASSED\n");
    printf("********************************\n");
    return 0;
}