/*
list.c

Author: Shravan Gupta
Date: 18-5-20
*/

#include "list.h"

//Initialize list nodes and heads
Node Nodes[LIST_MAX_NUM_NODES] = {[0 ... LIST_MAX_NUM_NODES-1].item = NULL, [0 ... LIST_MAX_NUM_NODES-1].next = NULL,
                                [0 ... LIST_MAX_NUM_NODES-1].prev = NULL, [0 ... LIST_MAX_NUM_NODES-1].presentIn = NULL};
List Lists[LIST_MAX_NUM_HEADS] = {[0 ... LIST_MAX_NUM_HEADS-1].head = NULL, [0 ... LIST_MAX_NUM_HEADS-1].tail = NULL,
                                [0 ... LIST_MAX_NUM_HEADS-1].curr =NULL, [0 ... LIST_MAX_NUM_HEADS-1].count = 0,
                                [0 ... LIST_MAX_NUM_HEADS-1].currBeforeStart = 0, [0 ... LIST_MAX_NUM_HEADS-1].currBeyondEnd = 0,
                                [0 ... LIST_MAX_NUM_HEADS-1].active = 0};


//Temp array to check for available nodes
List* availList[LIST_MAX_NUM_HEADS] = {[0 ... LIST_MAX_NUM_HEADS - 1] = NULL};
int availListCount = 0;

//Variables to keep track for no. of nodes and heads used
int nodesUsed = 0;
int listPos = 0;

// Makes a new, empty list, and returns its reference on success.
// Returns a NULL pointer on failure.
List* List_create(){

    //All the heads are exhausted, return NULL
    if(listPos >= LIST_MAX_NUM_HEADS)
        return NULL;

    //If there is node available
    if(availListCount != 0){
        List* newList = availList[availListCount - 1];
        availList[availListCount - 1] = NULL;
        newList->active = true;
        newList->count = 0;
        availListCount--;
        return newList;
    }
    else{
        Lists[listPos].active = true;
        List* newList = &(Lists[listPos]);
        listPos++;
        return newList;
    }
}

// Returns the number of items in pList.
int List_count(List* pList){

    return pList->count;
}

// Returns a pointer to the first item in pList and makes the first item the current item.
// Returns NULL and sets current item to NULL if list is empty.
void* List_first(List* pList){

    //Checking for possible errors
    if(pList->active == false || pList == NULL)
        return NULL;

    //If list is not empty and head exists
    if(pList->count != 0 && pList->head){
        pList->curr = pList->head;
        pList->currBeforeStart = false;
        pList->currBeyondEnd = false;
        return pList->head->item;
    }
    else{
        pList->curr = NULL;
        return NULL;
    }
}

// Returns a pointer to the last item in pList and makes the last item the current item.
// Returns NULL and sets current item to NULL if list is empty.
void* List_last(List* pList){

    //Checking for possible errors
    if(pList->active == false || pList == NULL)
        return NULL;

    //If list is not empty and tail exists
    if(pList->count != 0 && pList->tail){
        pList->curr = pList->tail;
        pList->currBeyondEnd = false;
        pList->currBeforeStart = false;
        return pList->tail->item;
    }
    else{
        pList->curr = NULL;
        return NULL;
    }
}

// Advances pList's current item by one, and returns a pointer to the new current item.
// If this operation advances the current item beyond the end of the pList, a NULL pointer
// is returned and the current item is set to be beyond end of pList.
void* List_next(List* pList){

    //Checking for possible errors
    if(pList->active == false || pList == NULL || pList->count == 0)
        return NULL;

    //If curr at tail, curr goes beyond plist and return NULL
    if(pList->curr == pList->tail){
        pList->curr = pList->tail->next;
        pList->currBeyondEnd = true;
        return NULL;
    }
    //If curr beyond the end, return NULL
    else if(pList->currBeyondEnd == true){
        pList->curr = pList->tail->next;
        return NULL;
    }
    //If curr before head, bring it back to head
    else if(pList->currBeforeStart == true){
        pList->curr = pList->head;
        pList->currBeforeStart = false;
        return pList->curr->item;
    }
    else{
        pList->curr = pList->curr->next;
        return pList->curr->item;
    }
}

// Backs up pList's current item by one, and returns a pointer to the new current item.
// If this operation backs up the current item beyond the start of the pList, a NULL pointer
// is returned and the current item is set to be before the start of pList.
void* List_prev(List* pList){

    //Checking for possible errors
    if(pList->active == false || pList == NULL || pList->count == 0)
        return NULL;

    //If curr at head, curr goes before head and return NULL
    if(pList->curr == pList->head){
        pList->curr = pList->head->prev;
        pList->currBeforeStart = true;
        return NULL;
    }
    //If curr before head, return NULL
    else if(pList->currBeforeStart == true){
        pList->curr = pList->head->prev;
        return NULL;
    }
    //If curr beyond tail, bring it back to tail
    else if(pList->currBeyondEnd == true){
        pList->curr = pList->tail;
        pList->currBeyondEnd = false;
        return pList->curr->item;
    }
    else{
        pList->curr = pList->curr->prev;
        return pList->curr->item;
    }
}

// Returns a pointer to the current item in pList.
void* List_curr(List* pList){

    //If curr if before start, beyong tail or list is empty, return NULL
    if(pList->currBeforeStart == true || pList->currBeyondEnd == true || pList->count == 0)
        return NULL;
    else
        return pList->curr->item;
}

// Adds the new item to pList directly after the current item, and makes item the current item.
// If the current pointer is before the start of the pList, the item is added at the start. If
// the current pointer is beyond the end of the pList, the item is added at the end.
// Returns 0 on success, -1 on failure.
int List_add(List* pList, void* pItem){

    //Checking for possible errors
    if(pList->active == false || pList == NULL || nodesUsed >= LIST_MAX_NUM_NODES)
        return -1;

    Node* newNode = Nodes + nodesUsed;
    newNode->item = pItem;
    newNode->presentIn = pList;
    nodesUsed++;

    //If list is empty
    if(pList->count == 0 || pList->head == NULL){
        pList->head = newNode;
        pList->tail = newNode;
        pList->curr = newNode;
        pList->count++;
        return 0;
    }
    //If curr is before the start
    else if(pList->currBeforeStart == true){
        pList->head->prev = newNode;
        newNode->next = pList->head;
        pList->head = newNode;
        pList->curr = newNode;
        pList->currBeforeStart = false;
        pList->count++;
        return 0;
    }
    //If curr is beyond the tail or at the tail
    else if(pList->currBeyondEnd == true || pList->curr == pList->tail){
        pList->tail->next = newNode;
        newNode->prev = pList->tail;
        pList->tail = newNode;
        pList->curr = newNode;
        pList->currBeyondEnd = false;
        pList->count++;
        return 0;
    }
    else{
        pList->curr->next = newNode;
        newNode->prev = pList->curr;
        newNode->next = pList->curr->next;
        pList->curr = newNode;
        pList->count++;
        return 0;
    }
    return -1;
}

// Adds item to pList directly before the current item, and makes the new item the current one.
// If the current pointer is before the start of the pList, the item is added at the start.
// If the current pointer is beyond the end of the pList, the item is added at the end.
// Returns 0 on success, -1 on failure.
int List_insert(List* pList, void* pItem){

    //Checking for possible errors
    if(pList->active == false || pList == NULL || nodesUsed >= LIST_MAX_NUM_NODES)
        return -1;

    Node* newNode = Nodes + nodesUsed;
    newNode->item = pItem;
    newNode->presentIn = pList;
    nodesUsed++;

    //If list is empty
    if(pList->count == 0 || pList->head == NULL){
        pList->head = newNode;
        pList->tail = newNode;
        pList->curr = newNode;
        pList->count++;
        return 0;
    }
    //If curr is before the start or at the start
    else if(pList->currBeforeStart == true || pList->curr == pList->head){
        pList->head->prev = newNode;
        newNode->next = pList->head;
        pList->head = newNode;
        pList->curr = newNode;
        pList->currBeforeStart = false;
        pList->count++;
        return 0;
    }
    //If curr is beyond the tail
    else if(pList->currBeyondEnd == true){
        pList->tail->next = newNode;
        newNode->prev = pList->tail;
        pList->tail = newNode;
        pList->curr = newNode;
        pList->currBeyondEnd = false;
        pList->count++;
        return 0;
    }
    else{
        pList->curr->prev = newNode;
        newNode->next = pList->curr;
        newNode->prev = pList->curr->prev;
        pList->curr = newNode;
        pList->count++;
        return 0;
    }
    return -1;
}

// Adds item to the end of pList, and makes the new item the current one.
// Returns 0 on success, -1 on failure.
int List_append(List* pList, void* pItem){

    //Checking for possible errors
    if(pList->active == false || pList == NULL || nodesUsed >= LIST_MAX_NUM_NODES)
        return -1;

    Node* newNode = Nodes + nodesUsed;
    newNode->item = pItem;
    newNode->presentIn = pList;
    nodesUsed++;

    //If list is empty
    if(pList->count == 0 || pList->head == NULL){
        pList->head = newNode;
        pList->tail = newNode;
        pList->curr = newNode;
        pList->count++;
        return 0;
    }
    else{
        pList->tail->next = newNode;
        newNode->prev = pList->tail;
        pList->tail = newNode;
        pList->curr = newNode;
        pList->currBeyondEnd = false;
        pList->count++;
        return 0;
    }
    return -1;
}

// Adds item to the front of pList, and makes the new item the current one.
// Returns 0 on success, -1 on failure.
int List_prepend(List* pList, void* pItem){

    //Checking for possible errors
    if(pList->active == false || pList == NULL || nodesUsed >= LIST_MAX_NUM_NODES)
        return -1;

    Node* newNode = Nodes + nodesUsed;
    newNode->item = pItem;
    newNode->presentIn = pList;
    nodesUsed++;

    //If list is empty
    if(pList->count == 0 || pList->head == NULL){
        pList->head = newNode;
        pList->tail = newNode;
        pList->curr = newNode;
        pList->count++;
        return 0;
    }
    else{
        pList->head->prev = newNode;
        newNode->next = pList->head;
        pList->head = newNode;
        pList->curr = newNode;
        pList->currBeforeStart = false;
        pList->count++;
        return 0;
    }
    return -1;
}

// Return current item and take it out of pList. Make the next item the current one.
// If the current pointer is before the start of the pList, or beyond the end of the pList,
// then do not change the pList and return NULL.
void* List_remove(List* pList) {

    //If curr if before start, beyong tail or list is empty, return NULL
    if(pList->count == 0 || pList->currBeforeStart == true || pList->currBeyondEnd == true)
        return NULL;

    //Node to be freed after we remove the item
    Node nodeToBeDeleted = *pList->curr;
    //Node at the end of the noed array
    Node* endNode = Nodes + nodesUsed - 1;
    //Variables to keep  track of curr
    bool atHead = false;
    bool atTail = false;

    //When there's only one item in the list
    if (pList->head == pList->tail && pList->count == 1)
    {
        pList->head = NULL;
        pList->tail = NULL;
        pList->active = false;
    }
    //If curr is at the head
    else if (pList->curr == pList->head) {
        pList->curr->next->prev = pList->head;
        pList->head = pList->head->next;
        pList->head->prev = NULL;
        atHead = true;
    }
    //If curr is at the tail
    else if (pList->curr == pList->tail) {
        pList->curr->prev->next = NULL;
        pList->tail = pList->tail->prev;
        pList->currBeyondEnd = true;
        atTail = true;
    }
    else {
        pList->curr->prev->next = pList->curr->next;
        pList->curr->next->prev = pList->curr->prev;
    }

    //Making node available for future use
    if (pList->curr != endNode ){

        *pList->curr = *endNode;
        List* editedList = endNode->presentIn;

        if (editedList->head == endNode)
            editedList->head = pList->curr;
        if (editedList->tail == endNode)
            editedList->tail = pList->curr;
        if (editedList->curr == endNode)
            editedList->curr = pList->curr;

        if (endNode->prev != NULL)
            endNode->prev->next = pList->curr;
        else if (endNode->next != NULL)
            endNode->next->prev = pList->curr;
    }
    //Delete the node
    endNode->item = NULL;
    endNode->next = NULL;
    endNode->prev = NULL;
    endNode->presentIn = NULL;
    nodesUsed--;
    pList->count--;

    //Adjusting the value of curr
    if (atHead == true){
        pList->curr = pList->head;
        pList->currBeforeStart = false;
    }
    else if(atTail == true)
        pList->curr = NULL;
    else
        pList->curr = nodeToBeDeleted.next;

    return nodeToBeDeleted.item;
}


// Adds pList2 to the end of pList1. The current pointer is set to the current pointer of pList1.
// pList2 no longer exists after the operation; its head is available
// for future operations.
void List_concat(List* pList1, List* pList2) {

    //If both the heads are present, concat the list
    if(pList1->head && pList2->head){
        pList1->tail->next = pList2->head;
        pList2->head->prev = pList1->tail;
        pList1->tail = pList2->tail;
    }
    //If list1 is empty, list2 is the resulting concat list
    else if(pList1->count == 0 && pList2->head != NULL){
        pList1->head = pList2->head;
        pList1->tail = pList2->tail;
        pList1->currBeforeStart = true;
        pList1->curr = NULL;
    }

    pList1->count += pList2->count;

    //Delete list 2 and make its head available
    if(pList2 == Lists + listPos -1)
        listPos--;
    else{
        availList[availListCount] = pList2;
        availListCount++;
    }
    pList2->head = NULL;
    pList2->tail = NULL;
    pList2->curr = NULL;
    pList2->currBeforeStart = false;
    pList2->currBeyondEnd = false;
    pList2->active = false;
    pList2->count = 0;
}

// Delete pList. pItemFreeFn is a pointer to a routine that frees an item.
// It should be invoked (within List_free) as: (*pItemFreeFn)(itemToBeFreedFromNode);
// pList and all its nodes no longer exists after the operation; its head and nodes are
// available for future operations.
// UPDATED: Changed function pointer type, May 19
typedef void (*FREE_FN)(void* pItem);
void List_free(List* pList, FREE_FN pItemFreeFn) {

    listPos--;

     //Checking for possible errors
    if(pList->count == 0)
        return;

    pList->curr = pList->head;
    pList->currBeforeStart = false;

    // Loop through the list
    while (pList->head && pList->curr != NULL){

        //Iterate through each node and remove them
        pList->curr = pList->head;
        if(pList->head->item)
            (*pItemFreeFn)(pList->head->item);
        List_remove(pList);
    }

    //Delete the list and make its head available
    if(pList == Lists + listPos -1)
        listPos--;
    else{
        availList[availListCount] = pList;
        availListCount++;
    }
    pList->head = NULL;
    pList->tail = NULL;
    pList->curr = NULL;
    pList->currBeforeStart = false;
    pList->currBeyondEnd = false;
    pList->active = false;
    pList->count = 0;
    pList = NULL;

}

// Return last item and take it out of pList. Make the new last item the current one.
// Return NULL if pList is initially empty.
void* List_trim(List* pList) {

     //Checking for possible errors
    if(pList->count == 0)
        return NULL;

    //Move curr to the last item
    pList->curr = pList->tail;
    pList->currBeyondEnd = false;

    //Node to be freed after we remove the item
    Node nodeToBeDeleted = *pList->curr;
    //Node at the end of the noed array
    Node* endNode = Nodes + nodesUsed - 1;

    //When there's only one item in the list
    if (pList->head == pList->tail && pList->count == 1)
    {
        pList->head = NULL;
        pList->tail = NULL;
        //pList->active = false;
    }
    //If curr is at the tail
    else {
        pList->tail = pList->tail->prev;
        pList->tail->next = NULL;
    }

    //Making node available for future use
    if (pList->curr != endNode ){

        *pList->curr = *endNode;
        List* editedList = endNode->presentIn;

        if (editedList->head == endNode)
            editedList->head = pList->curr;
        if (editedList->tail == endNode)
            editedList->tail = pList->curr;
        if (editedList->curr == endNode)
            editedList->curr = pList->curr;

        if (endNode->prev != NULL)
            endNode->prev->next = pList->curr;
        if (endNode->next != NULL)
            endNode->next->prev = pList->curr;
    }
    //Delete the node
    endNode->item = NULL;
    endNode->next = NULL;
    endNode->prev = NULL;
    endNode->presentIn = NULL;
    nodesUsed--;
    pList->count--;
    pList->curr = nodeToBeDeleted.prev;

    return nodeToBeDeleted.item;
}

// Search pList, starting at the current item, until the end is reached or a match is found.
// In this context, a match is determined by the comparator parameter. This parameter is a
// pointer to a routine that takes as its first argument an item pointer, and as its second
// argument pComparisonArg. Comparator returns 0 if the item and comparisonArg don't match,
// or 1 if they do. Exactly what constitutes a match is up to the implementor of comparator.
//
// If a match is found, the current pointer is left at the matched item and the pointer to
// that item is returned. If no match is found, the current pointer is left beyond the end of
// the list and a NULL pointer is returned.
//
// UPDATED: Added clarification of behaviour May 19
// If the current pointer is before the start of the pList, then start searching from
// the first node in the list (if any).
typedef bool (*COMPARATOR_FN)(void* pItem, void* pComparisonArg);
void* List_search(List* pList, COMPARATOR_FN pComparator, void* pComparisonArg) {

     //Checking for possible errors
    if(pList->count == 0)
        return NULL;

    //If curr is before the start of the list
    if(pList->currBeforeStart == true){
        pList->curr = pList->head;
        pList->currBeforeStart = false;
    }

    // Loop through the list
    while( pList->curr != NULL){

        //Match found
        if(pComparator(pList->curr->item, pComparisonArg) == true)
            return pList->curr->item;
        //Continue Searching
        else
            pList->curr = pList->curr->next;
    }
    //Match not found
    pList->currBeyondEnd = true;
    pList->curr = NULL;
    return NULL;
}
