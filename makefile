
run: list.o main.o	
	gcc -g -Wall -Werror -o run list.o main.o

list.o: list.c list.h
	gcc -g -c list.c

main.o: main.c
	gcc -g -c main.c

clean:
	rm *.o run